module.exports = (sequelize, Sequelize) => {
    const Intervention = sequelize.define("Intervention", {
        id: {
            type: Sequelize.STRING,
            primaryKey: true
        },
        companyName: {
            type: Sequelize.STRING
        },
        address: {
            type: Sequelize.STRING
        },
        phoneNumber: {
            type: Sequelize.INTEGER
        },
        status: {
            type: Sequelize.STRING,
            defaultValue: 'En attente'
        }
    });

    Intervention.associate = (models) => {
        Intervention.hasMany(models.Vehicle, { foreignKey: 'InterventionId' });
    };

    return Intervention;
};
