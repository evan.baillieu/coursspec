const { authJwt } = require("../middleware");
const controller = require("../controllers/Intervention.controller");

module.exports = function (app) {
    app.use(function (req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    app.get(
        "/api/getallIntervention",
        controller.getIntervention
    );

    app.get(
        "/api/getIntervention/:id",
        controller.getRecord
    );

    app.post(
        "/api/createIntervention",
        controller.createIntervention
    );

    app.delete(
        "/api/deleteIntervention/:id",
        controller.deleteIntervention
    );

    app.patch(
        "/api/updateIntervention/:id",
        controller.updateInterventionStatus
    )
};