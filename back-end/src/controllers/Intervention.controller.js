const db = require("../models");
const uuid = require('uuid');
const Intervention = db.Intervention;

exports.getIntervention = (req, res) => {
    Intervention.findOne({
        where: {
            id: req.params.id
        }
    })
        .then(intervention => {
            if (!intervention) {
                return res.status(404).send({ message: "Intervention Not found." });
            }

            res.status(200).send(intervention);
        })
        .catch(err => {
            res.status(500).send({ message: err.message });
        });
};

exports.getInterventions = (req, res) => {
    Intervention.findAll()
        .then(interventions => {
            res.status(200).send(interventions);
        })
        .catch(err => {
            res.status(500).send({ message: err.message });
        });
};

exports.createIntervention = async (req, res) => {
    try {
        const newIntervention = await Intervention.create({
            id: uuid.v4(),
            companyName: req.body.companyName,
            address: req.body.address,
            phoneNumber: req.body.phoneNumber,
            createdAt: new Date(),
            updatedAt: new Date()
        });
        res.status(201).send({
            id: newIntervention.id
        });
    } catch (err) {
        res.status(500).send({ message: err.message });
    }
};

exports.deleteIntervention = (req, res) => {
    Intervention.destroy({
        where: {
            id: req.params.id
        }
    })
        .then(() => {
            res.status(200).send({ message: "Intervention has been deleted" });
        })
        .catch(err => {
            res.status(500).send({ message: err.message });
        });
};

exports.updateInterventionStatus = (req, res) => {
    Intervention.update(
        { status: newStatus },
        {
            where: { id: req.params.id }
        }
    )
        .then((result) => {
            if (result[0] === 1) {
                res.status(200).send({ message: "Statut mis à jour avec succès." });
            } else {
                res.status(404).send({ message: "Intervention non trouvée." });
            }
        })
        .catch(err => {
            res.status(500).send({ message: err.message });
        });
};