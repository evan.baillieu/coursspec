import Moto from "../assets/image/MotoPhoto.png";

export const getVehicules = () => {
    return [
        {
            id: '1',
            photo: Moto,
            nom: 'Moto A'
        },
        {
            id: '2',
            photo: Moto,
            nom: 'Voiture B'
        },
        {
            id: '3',
            photo: Moto,
            nom: 'Camion C'
        }
    ];
};
