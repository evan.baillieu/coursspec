import { getVehicules } from "../service/vehiculeService";
import CartVehicule from "./CartVehicule/CartVehicule";
import StatusGoodIcon from "../assets/svg/StatusGood.svg";
import StatusNotGoodIcon from "../assets/svg/StatusNotGood.svg";

export default function Test() {
  const vehicules = getVehicules();

  return (
    <ul>
      {vehicules.map((vehicule) => (
        <CartVehicule
          key={vehicule.id}
          StatusGood={StatusGoodIcon}
          StatusNotGood={StatusNotGoodIcon}
          {...vehicule}
        />
      ))}
    </ul>
  );
}
