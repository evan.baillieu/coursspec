import { useState } from "react";
import "./CartVehicule.sass";

export default function CartVehicule({
  photo,
  nom,
  StatusGood,
  StatusNotGood,
}) {
  const [isStatusGood, setIsStatusGood] = useState(true);
  const toggleStatus = () => setIsStatusGood((prev) => !prev);

  return (
    <li className="cartMoto">
      <img src={photo} alt={`Photo de ${nom}`} />

      <p>{nom}</p>
      <button onClick={toggleStatus}>
        <img
          src={isStatusGood ? StatusGood : StatusNotGood}
          alt={isStatusGood ? "Status Good icon" : "Status Not Good icon"}
        />
      </button>
    </li>
  );
}
